pnscan (1.14.1-2) unstable; urgency=medium

  * Fix d/watch issue
  * d/control:
    - Bump Standards-Version to 4.6.1.0
  * d/copyright:
    - Update copyright year

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 20 Dec 2022 22:02:44 +0800

pnscan (1.14.1-1) unstable; urgency=medium

  * Import new upstream
  * Remove unneeded patch
  * Use the manpage from upstream release
  * d/control:
    - Update copyright information with new upstream release
  * d/install:
    - Remove ipsort and add t_listen with new upstream release
  * d/rules:
    - Remove unneeded settings

 -- SZ Lin (林上智) <szlin@debian.org>  Wed, 01 Jul 2020 16:15:33 +0800

pnscan (1.13-1) unstable; urgency=medium

  [ Raphaël Hertzog ]
  * Add patch to inject LDFLAGS for proper hardening support

  [ Debian Janitor ]
  * Add package metadata

  [ Samuel Henrique ]
  * Configure git-buildpackage for Debian
  * Add Salsa's CI configuration file

  [ SZ Lin (林上智) ]
  * Import new upstream
  * Add autopkgtest case
  * Tidy the patch due to new upstream release
  * d/control:
    - Bump debhelper-compat to 13
    - Bump Standards-Version to 4.5.0

 -- SZ Lin (林上智) <szlin@debian.org>  Tue, 09 Jun 2020 17:04:44 +0800

pnscan (1.12+git20180612-2) unstable; urgency=low

  [ Helmut Grohne ]
  * Fix FTCBFS: Let dh_auto_build pass cross tools to make and pass
    CC via LNX_CC. (Closes: #942204)

  [ SZ Lin (林上智) ]
  * d/control:
    - Use debhelper-compat (= 12) in build-dependency to replace d/compat
    - Set Rules-Requires-Root: no
    - Bump Standards-Version to 4.4.1
  * d/watch:
    - Fix malformed flemamemangle

 -- SZ Lin (林上智) <szlin@debian.org>  Sun, 13 Oct 2019 00:08:02 +0800

pnscan (1.12+git20180612-1) unstable; urgency=medium

  [ Raphaël Hertzog ]
  * d/control:
    - Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org
    - Update team maintainer address to Debian Security Tools

  [ SZ Lin (林上智) ]
  * Import new upstream
  * Fixed coredump with missing arg (Closes: #716145)
  * d/control:
    - Bump Standards-Version to 4.2.1
    - Bump debhelper version to 11
    - Remove Builde-Depends: dh-systemd
    - Update email of uploader
  * d/copyright:
    - Replace "http" with "https"
    - Update copyright info.
  * d/compat:
    - Bump version to 11
  * d/rules:
    - Remove "--with systemd --parallel" for dh since it is enabled by
      default now.
  * Converting git-dpm to gbp pq

 -- SZ Lin (林上智) <szlin@debian.org>  Mon, 27 Aug 2018 14:25:08 +0800

pnscan (1.12-1) unstable; urgency=medium

  * New maintainer (Closes: #847510)
  * Import new upstream
  * Bump Standards-Version to 3.9.8
  * Bump debhelper version to 10
  * Rewrite copyright format
  * Add GIT address at the control file
  * Rewrite debian/rules by using dh command

 -- SZ Lin (林上智) <szlin@cs.nctu.edu.tw>  Sun, 11 Dec 2016 22:53:52 +0800

pnscan (1.11-6) unstable; urgency=low

  * Removed option '-s' in debian/rules for nostrip (Closes: #437778).

 -- William Vera <billy@billy.com.mx>  Thu, 08 Jul 2010 17:30:23 -0500

pnscan (1.11-5) unstable; urgency=low

  * New maintainer (Closes: #586411).
  * Bump Standards-Version to 3.9.0.
  * Bump debhelper version to 7.0.50 at the control file.
  * Updated clean rule in the debian/rules.
  * Added  ${misc:Depends} as depend in control file.
  * Switch to dpkg-source 3.0 (quilt) format.
  * Added Homepage field at control file.
  * Updated watch file (Closes: #529133).
  * Added DM Upload Allowed at the control file.

 -- William Vera <billy@billy.com.mx>  Thu, 01 Jul 2010 09:50:54 -0500

pnscan (1.11-4) unstable; urgency=low

  * Applied patch from Jose Luis Gonzalez <jlgonzal@ya.com> to correct a
    segfault when no arguments are used, closes: #447408.
  * Changed from debhelper compatibility 3 to 4.
  * Updated standards version from 3.5.8 to 3.7.2.

 -- Ola Lundqvist <opal@debian.org>  Sun, 21 Oct 2007 12:59:15 +0200

pnscan (1.11-3) unstable; urgency=low

  * Fixed errno clobber using patch from Florian Weimer.
  * Fixed socket problem using patch from Florian Weimer.
  * Updated standards version from 3.5.2 to 3.5.8.
  * Fixed first line of description.

 -- Ola Lundqvist <opal@debian.org>  Fri, 18 Jul 2003 12:10:00 +0200

pnscan (1.11-2) unstable; urgency=low

  * Fixed build dependency, closes: #145156.

 -- Ola Lundqvist <opal@debian.org>  Tue, 30 Apr 2002 09:56:29 +0200

pnscan (1.11-1) unstable; urgency=low

  * New upstream release, closes: #139451.
  * Updated description, closes: #144908.

 -- Ola Lundqvist <opal@debian.org>  Mon, 29 Apr 2002 08:54:10 +0200

pnscan (1.6-1) unstable; urgency=low

  * Initial Release.

 -- Ola Lundqvist <opal@debian.org>  Fri, 22 Mar 2002 10:53:01 +0100
